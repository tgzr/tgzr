from __future__ import annotations

import os
from functools import wraps

import pydantic


_DOTENV_PATH = None
_SETTINGS_REGISTRY = {}
_SETTINGS_MODELS = None


def settings(name: str = ..., weight: int = 100):
    """
    Decorator for pydantic.BaseSettings class to register as
    settings.

    If name is not provided, the module name of the decorated
    object will be used.

    Example:

        @settings("my_app")
        class MyAppSettings(pydantic.BaseSettings):
            awesome = True
            how_much: int = 100

    """

    @wraps(register_settings)
    def decorator(settings_model):
        register_settings(settings_model, name=name, weight=weight)
        return settings

    return decorator
    # return partial(register_settings, name=name, weight=weight)


def register_settings(
    settings_model: pydantic.BaseSettings, name: str, weight: int = 100
) -> None:
    """
    Register a settings model at the given name.

    The weight defines the settings overriding order: when
    several settings register on the same name, the bigger weights
    override the lower weights.

    If a setting is already registered on this name with this weight,
    the weight is automatically increased by one.
    """
    global _SETTINGS_REGISTRY, _SETTINGS_MODELS
    if name is Ellipsis:
        name = settings_model.__module__
    while (weight, name) in _SETTINGS_REGISTRY:
        weight += 1
    _SETTINGS_REGISTRY[(weight, name)] = settings_model
    # Invalidate all models so that overrides are recomputed:
    _SETTINGS_MODELS = None


def _build_settings() -> pydantic.BaseModel:
    settings = dict()
    for (weight, path), settings_model in sorted(_SETTINGS_REGISTRY.items()):
        try:
            base = settings[path]
        except KeyError:
            pass
        else:
            name = settings_model.schema()["title"] + "|" + base.schema()["title"]
            settings_model = type(name, (settings_model, base), {})
        settings[path] = settings_model
    return settings


def set_dotenv_path(path):
    global _DOTENV_PATH
    _DOTENV_PATH = path


def get_settings(path: str) -> pydantic.BaseModel:
    global _SETTINGS_MODELS
    if _SETTINGS_MODELS is None:
        _SETTINGS_MODELS = _build_settings()
    model = _SETTINGS_MODELS[path]
    return model()


def test():
    @settings("tgzr.mod1")
    class Model1(pydantic.BaseSettings):
        a: str = "yolo"
        b = False

    @settings("tgzr.mod1", weight=0)
    class Model1_over(pydantic.BaseSettings):
        b = True
        c: int = 3.14

    class Model2(pydantic.BaseSettings):
        a: str = "takavoir"
        c = 123

    # Also without the decorators:
    register_settings(Model2, "tgzr.mod2")

    s = get_settings("tgzr.mod1")
    print(s.schema_json(indent=2))
    print(">>>", s.a, s.b, s.c)

    os.environ["a"] = "BIM"
    s = get_settings("tgzr.mod2")
    print(">>>", s.a, '!= "takavoir"')


if __name__ == "__main__":
    test()
