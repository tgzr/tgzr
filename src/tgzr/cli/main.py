import os
import click
import pluggy

from . import plugin_specs
from .lib import packages, settings

from ..settings import set_dotenv_path


def _cli_set_env_var(ctx, param, values):
    for argv in values:
        name, value = argv.split("=", 1)
        click.echo(f"overridding env var: {name}={value}")
        os.environ[name] = value


def _cli_set_dotenv_path(ctx, param, value):
    set_dotenv_path(value)


@click.group()
@click.option(
    "--env",
    multiple=True,
    help="""
Where TEXT is in the form "VarName=Value". Allowed several times.

Overrides a environment value. See the settings command for environemnt variable usage.
""",
    callback=_cli_set_env_var,
)
@click.option(
    "--dotenv",
    help="Set the .env file to use (default is $pwd/.env).",
    callback=_cli_set_dotenv_path,
)
def main_group(env, dotenv):
    """ """


def main():
    pm = pluggy.PluginManager("tgzr.cli")
    pm.add_hookspecs(plugin_specs)
    pm.load_setuptools_entrypoints("tgzr.cli")
    pm.register(packages)
    pm.register(settings)

    hook = pm.hook
    results = hook.add_cli_group(main_group=main_group)

    main_group()
