import importlib
import importlib.metadata

import click

from tgzr.cli import cli_plugin


@cli_plugin
def add_cli_group(main_group: click.Group):
    click.option()
    return main_group.add_command(packages)


@click.group()
def packages():
    pass


@packages.command()
@click.option(
    "--show-version", "-v", default=False, is_flag=True, help="Show packages version."
)
def list(show_version=False):
    """
    List the tgzr related packages currently installed.
    """
    # TODO: make this work for real by listing packages with one of tgzr supported entry points
    package_names = (
        "tgzr.dshell",
        "tgzr.cui",
    )
    click.echo("List of installed tgzr packages:")
    for package_name in package_names:
        try:
            module = importlib.import_module(package_name)
        except ImportError:
            pass
        else:
            version_display = ""
            if show_version:
                try:
                    version_string = importlib.metadata.version(package_name)
                except importlib.metadata.PackageNotFoundError as err:
                    version_string = "?!?!"
                version_display = f"=={version_string}"
            click.echo(f"  {package_name}{version_display} @ {module.__file__}")


@packages.command()
@click.argument("package_name")
def version(package_name):
    """Show the version of a package."""
    try:
        version_string = importlib.metadata.version(package_name)
    except importlib.metadata.PackageNotFoundError as err:
        raise click.ClickException(err)
    click.echo(f"{package_name}=={version_string}")


@packages.command()
@click.argument("package_name")
@click.option("--upgrade", "-u", help="Install newer version if available")
def install(package_name, upgrade):
    """Install or update a package."""
    click.echo("Package install not yet supported :/")
    click.echo(f"Maybe try `pip install {package_name}` ?")
