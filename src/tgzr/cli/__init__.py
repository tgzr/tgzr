import sys

import pluggy

cli_plugin = pluggy.HookimplMarker("tgzr.cli")
"""Marker to declare plugin implementation."""
